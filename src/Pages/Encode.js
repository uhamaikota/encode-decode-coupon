import React, { useState } from 'react'
import moment from 'moment'

function Encode() {
    const [customerName, setCustormerName] = useState('')
    const [storeName, setStoreName] = useState('')
    const [date, setDate] = useState('')
    const [data, setData] = useState()

    const encoder=(datearg,monthArg,yearArg,storeArg,customerArg)=> {
        let date = datearg.toString().padStart(2, "0")
        let month = monthArg.toString().padStart(2, "0")
        let year = yearArg.toString()
        let store = storeArg;
        let storecode = store.toString().padStart(3, "0");
        let customer = customerArg
        let customercode = customer.toString().padStart(5, '0');
        let n = parseInt(date + month + year + storecode + customercode)
        console.log("number ", n)
        if (n === 0) {
            return '0';
        }
        var digits = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var result = '';
        while (n > 0) {
            result = digits[n % digits.length] + result;
            let temp = n / digits.length
            n = parseInt(temp.toString(), 10);
        }
        console.log("1", result)

        return result;
    }


    const submit = () => {
        let datenumber = moment(date).format('D')
        let month = moment(date).format('M')
        let year = moment(date).format('YYYY')

        let result=encoder(datenumber, month, year, storeName, customerName)
        setData(result)
    }

    return (
        <>
            <br />
            <br />
            <br />
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">

                    </div>
                    <div class="col-sm-4">

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Customer Number</label>
                            <input required onChange={(e) => setCustormerName(e.target.value)} type="name" placeholder="Enter Customer Number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Store Number</label>
                            <input required onChange={(e) => setStoreName(e.target.value)} type="name" placeholder="Enter Store Number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Date</label>
                            <input required onChange={(e) => setDate(e.target.value)} type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        </div>
                        <button onClick={submit} type="submit" class="btn btn-primary">Submit</button>

                        <br />
                        <br />
                        <br />

                            Generated Code: {data}

                            
                        <br />
                        <br />
                        <br />
                        <div style={{textAlign:"center"}}>
                            <a class="btn btn-primary" style={{textDecoration:"none",fontSize:"18px"}} href="/">Generate Code</a> <a class="btn btn-primary" style={{textDecoration:"none",fontSize:"18px"}} href="/decode">Check Code</a>
                        </div>
                    </div>
                    <div class="col-sm-4">

                    </div>
                </div>
            </div>
        </>
    )
}

export default Encode
