import moment from 'moment'
import React, { useState } from 'react'

function Decode() {
    const [code, setCode] = useState('')
    const [email, setEmail] = useState('')
    const [output, setOutput] = useState({})

    const submit = () => {
        let result=decoder(code)

        setOutput(result)
    }

    
    const decoder=(s)=>{
        var digits = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var result = 0;
        for (var i=0 ; i<s.length ; i++) {
          var p = digits.indexOf(s[i]);
          if (p < 0) {
            return NaN;
          }
          result += p * Math.pow(digits.length, s.length - i - 1);
        }
        console.log("2",result)
        let day = result.toString().substr(0,2);
        let month = result.toString().substr(2,2);
        let year = result.toString().substr(4,4);
        let store = result.toString().substr(8,3);
        
        let customer = result.toString().substr(11,5);
        console.log(day)
        console.log(month)
        console.log(year)
        console.log(store)
        console.log(customer)
        
        return {
            date:day+'-'+month+'-'+year,
            store:store,
            customer:customer
        }
      }

    return (
        <>
            <br />
            <br />
            <br />
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">

                    </div>
                    <div class="col-sm-4">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email</label>
                            <input onChange={(e) => setEmail(e.target.value)} type="email" placeholder="Enter Email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Enter Code</label>
                            <input onChange={(e) => setCode(e.target.value)} type="email" placeholder="Enter Code" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        </div>

                        <button type="submit" onClick={submit} class="btn btn-primary">Submit</button>
                        <br />
                        <br />
                        <div class="card" style={{ width: "18rem" }}>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Customer Number: {parseInt(output.customer)}</li>
                                <li class="list-group-item">Store Number: {parseInt(output.store)}</li>
                                <li class="list-group-item">Date: {output.date}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">

                    </div>
                </div>
            </div>
        </>
    )
}

export default Decode
